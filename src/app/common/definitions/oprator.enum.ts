export enum OperatorEnum {
    DIVISION = '/',
    ADDITION = '+',
    SUBSTRACTION = '-',
    MULTIPLICATION = '*',
    UNDEFINED = '',
}