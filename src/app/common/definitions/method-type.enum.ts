export enum MethodTypeEnum {
    SET_NUMBER = 0,
    SET_OPERATOR = 1,
    DECIMAL = 2,
    CALCULATE = 3,
    CLEAR = 4,
}