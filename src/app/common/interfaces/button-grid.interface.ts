import { MethodTypeEnum } from '../definitions/method-type.enum'

export interface IButtonGrid {
    text: string;
    cols: number;
    rows: number;
    value?: number;
    methodType: MethodTypeEnum;
    color: string;
}