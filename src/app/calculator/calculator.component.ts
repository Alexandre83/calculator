import { Component, OnInit } from '@angular/core';
import { MethodTypeEnum } from '../common/definitions/method-type.enum';
import { OperatorEnum } from '../common/definitions/oprator.enum';
import { IButtonGrid } from '../common/interfaces/button-grid.interface';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  buttonsGrid: IButtonGrid[] = [
    { text: '+', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_OPERATOR },
    { text: '-', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_OPERATOR },
    { text: 'x', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_OPERATOR },
    { text: '/', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_OPERATOR },
    { text: '7', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '8', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '9', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '=', cols: 1, rows: 4, color: 'white', methodType: MethodTypeEnum.CALCULATE },
    { text: '4', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '5', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '6', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '1', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '2', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '3', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '0', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.SET_NUMBER },
    { text: '.', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.DECIMAL },
    { text: 'AC', cols: 1, rows: 1, color: 'white', methodType: MethodTypeEnum.CLEAR },
  ]
  currentNumbers: string[] = ['0', '0'];
  display = '0';
  currentNumberIndex = 0;
  operator: OperatorEnum = OperatorEnum.UNDEFINED;
  finalResult = 0;
  readyToClear = false;

  constructor() { }

  ngOnInit(): void {
  }

  public setNumber(value: string): void {
    if (this.readyToClear) {
      this.clear();
    }

    if (this.currentNumbers[this.currentNumberIndex] === '0') {
      this.currentNumbers[this.currentNumberIndex] = value;
    } else {
      this.currentNumbers[this.currentNumberIndex] += value;
    }

    this.display = this.display === '0' ? value : this.display + value;
  }

  public setDecimal(): void {
    if(!this.currentNumbers[this.currentNumberIndex].includes('.')) {
      this.currentNumbers[this.currentNumberIndex] += '.';
      this.display += '.';
    }
  }

  public setOperator(operator: string): void {
    this.operator = operator as OperatorEnum;
    this.currentNumberIndex = 1;
    this.display += ` ${operator} `
  }

  public calculate(): void {
    this.finalResult = this.doCalculation();
    this.display = this.finalResult.toString();
    this.readyToClear = true;
  }

  public doCalculation(): number {
    switch(this.operator) {
      case OperatorEnum.ADDITION:
        return +this.currentNumbers[0] + +this.currentNumbers[1];
      case OperatorEnum.SUBSTRACTION:
        return +this.currentNumbers[0] - +this.currentNumbers[1];
      case OperatorEnum.MULTIPLICATION:
        return +this.currentNumbers[0] * +this.currentNumbers[1];
      case OperatorEnum.DIVISION:
        return +this.currentNumbers[0] / +this.currentNumbers[1];
      default:
        return +this.currentNumbers[0];
    }
  }

  public clear() {
    this.currentNumberIndex = 0;
    this.currentNumbers = ['0', '0']
    this.finalResult = 0;
    this.operator = OperatorEnum.UNDEFINED;
    this.display = '0';
    this.readyToClear = false;
  }

  public bindMethodType(methodType: MethodTypeEnum, value?: string) {
    switch(methodType) {
      case MethodTypeEnum.SET_NUMBER:
        value && this.setNumber(value);
        break;
      case MethodTypeEnum.SET_OPERATOR:
        value && this.setOperator(value);
        break;
      case MethodTypeEnum.DECIMAL:
        this.setDecimal();
        break;
      case MethodTypeEnum.CLEAR:
        this.clear();
        break;
      case MethodTypeEnum.CALCULATE:
        this.calculate();
        break;
      default:
        throw new Error('Invalid method type.')
    }
  }

}
